from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

db = SQLAlchemy()

def create_all(app,db_uri):
    if app:
        with app.app_context():
            db.create_all()
    else:
        engine = create_engine(db_uri,echo=True)
        Base = declarative_base()
        Base.metadata.create_all(engine)