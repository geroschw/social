from typing import List
from flask import jsonify

from datos.manager import db
from dominio.perfil import Perfil


class PerfilesRepoSql:
    def get_all(self) -> List[Perfil]:
        return Perfil.query.all()

    def agregar(self,perfil):
        db.session.add(perfil)
        db.session.commit()

    def modificar(self,id,jsonData) -> bool:
        modified = Perfil.query.filter_by(id=id).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,id) -> bool:
        old = self.buscar_perfil(id)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_perfil(self,id) -> Perfil:
        return Perfil.query.get(id)
