from typing import List
from flask import jsonify

from datos.manager import db
from dominio.cuenta import Cuenta


class CuentasRepoSql:
    def get_all(self) -> List[Cuenta]:
        return Cuenta.query.all()

    def agregar(self,cliente):
        db.session.add(cliente)
        db.session.commit()

    def modificar(self,id,jsonData) -> bool:
        modified = Cuenta.query.filter_by(id=id).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,id) -> bool:
        old = self.buscar_cuenta(id)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_cuenta(self,id) -> Cuenta:
        return Cuenta.query.get(id)

    def get_login(self,email,contrasenia):
        return db.session.query(Cuenta).filter(Cuenta.email==email).first()
        

        
#,Cuenta.contraseña==contrasenia