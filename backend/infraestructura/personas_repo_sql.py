from typing import List
from flask import jsonify

from datos.manager import db
from dominio.persona import Persona


class PersonasRepoSql:
    def get_all(self) -> List[Persona]:
        return Persona.query.all()

    def agregar(self,persona):
        db.session.add(persona)
        db.session.commit()

    def modificar(self,id,jsonData) -> bool:
        modified = Persona.query.filter_by(id=id).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,id) -> bool:
        old = self.buscar_persona(id)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_persona(self,id) -> Persona:
        return Persona.query.get(id)
