from typing import Dict

from datos.manager import db

class Persona(db.Model):
    __tablename__ = 'personas'

    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    nombre = db.Column(db.String(50),nullable=False)
    edad = db.Column(db.Integer)
    sexo = db.Column(db.String(6))
    


    def as_json(self) -> Dict:
        dictionary = self.dict.copy()
        del dictionary['_sa_instance_state']
        return dictionary