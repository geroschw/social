from typing import Dict

from datos.manager import db

class Datos(db.Model):
    __tablename__ = 'datos'

    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    email = db.Column(db.String(30),nullable=False)
    contraseña = db.Column(db.String(30),nullable=False)
    pseudonimo = db.Column(db.String(30),nullable=False)
    nombre = db.Column(db.String(50),nullable=False)

    def as_json(self) -> Dict:
        dictionary = self.dict.copy()
        del dictionary['_sa_instance_state']
        return dictionary