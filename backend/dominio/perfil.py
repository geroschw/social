from typing import Dict

from datos.manager import db

class Perfil(db.Model):
    __tablename__ = 'perfiles'

    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    pseudonimo = db.Column(db.String(30),nullable=False)
    descripcion = db.Column(db.String(100))
    seguidores = db.Column(db.Integer)
    seguidos = db.Column(db.Integer)
    publicaciones = db.Column(db.Integer)
    

    def as_json(self) -> Dict:
        dictionary = self.dict.copy()
        del dictionary['_sa_instance_state']
        return dictionary