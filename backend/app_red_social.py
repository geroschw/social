from flask import Flask
from flask_cors import CORS
from flask_restx import Api

from datos.manager import db
from datos.manager import create_all
from configuracion.config import config_app

from api.cuentas_api import nsCuentas
from api.perfiles_api import nsPerfiles

app = Flask(__name__)
config_app(app)
app.config['PROPAGATE_EXCEPTIONS'] = True

CORS(app)

 
api = Api(app,
        version='1.0',
        title='Fake instagram',
        description='-')

api.add_namespace(nsCuentas)
api.add_namespace(nsPerfiles)

db.init_app(app)
create_all(app,app.config['SQLALCHEMY_DATABASE_URI'])
app.config['SQLALCHEMY_ECHO'] = True

if __name__== '__main__':
    print("El servicio está corriendo en la siguiente configuración:")
    print(app.config)
    app.run(debug=False,use_reloader=False)