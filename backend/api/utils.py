from flask_restx import Model,fields,reqparse

ModeloCuenta = Model('Cuenta',{
    'email':fields.String(required=True),
    'contraseña':fields.String(required=True),
    'id':fields.Integer(required=True)
})

ModeloIdCuenta = Model('Cuenta',{
    'id':fields.Integer(required=True)
})

parserCuenta = reqparse.RequestParser(bundle_errors=True)
parserCuenta.add_argument('email',type=str,required=True,location='json')
parserCuenta.add_argument('contraseña',type=str)

parserEditarCuenta = parserCuenta.copy()
parserEditarCuenta.add_argument(
    'id',type=int,required=True,location='json')

def row2dict(row):
    return{
        c.name: str(getattr(row,c.name))
        for c in row.table.columns
    }

class CustomField(fields.Raw):
    def format(self,value):
        if isinstance(value,list):
            return [(v.as_dict()) for v in value]
        else:
            return value.as_json()

ModeloDatos = Model('DATOS',{
    'email':fields.String(required=True),
    'contraseña':fields.String(required=True),
    'pseudonimo': fields.String(required=True),
    'nombre': fields.String(required=True),
    'id':fields.Integer(required=True)
})

parserDatos = reqparse.RequestParser(bundle_errors=True)
parserDatos.add_argument('email',type=str,required=True,location='json')
parserDatos.add_argument('contraseña',type=str,required=True)
parserDatos.add_argument('pseudonimo',type=str,required=True)
parserDatos.add_argument('nombre',type=str,required=True)

ModeloPerfil = Model('Perfil',{
    'pseudonimo':fields.String(required=True),
    'descripcion':fields.String(required=True),
    'seguidores':fields.Integer(required=True),
    'seguidos':fields.Integer(required=True),
    'publicaciones':fields.Integer(required=True),
    'id':fields.Integer(required=True)
})

ModeloIdPerfil = Model('Perfil',{
    'id':fields.Integer(required=True)
})

parserPerfil = reqparse.RequestParser(bundle_errors=True)
parserPerfil.add_argument('pseudonimo',type=str,required=True,location='json')
parserPerfil.add_argument('descripcion',type=str)
parserPerfil.add_argument('seguidores',type=str)
parserPerfil.add_argument('seguidos',type=str)
parserPerfil.add_argument('publicaciones',type=str)

parserEditarPerfil = parserPerfil.copy()
parserEditarPerfil.add_argument(
    'id',type=int,required=True,location='json')