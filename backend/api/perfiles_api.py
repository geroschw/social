from flask import Flask
from flask_restx import Namespace,Resource,Model,fields,reqparse

from api.utils import ModeloPerfil,parserPerfil,ModeloIdPerfil,parserEditarPerfil
from dominio.perfil import Perfil
from infraestructura.perfiles_repo_sql import PerfilesRepoSql

nsPerfiles = Namespace(
    'perfiles',description='Administracion de perfiles',path='/perfiles')

nsPerfiles.models[ModeloPerfil.name] = ModeloPerfil

repoPerfiles = PerfilesRepoSql()


@nsPerfiles.route('/')
class ClientesResource(Resource):
    @nsPerfiles.marshal_list_with(ModeloPerfil)
    def get(self):
        return repoPerfiles.get_all()


#    @nsPerfiles.marshal_with(ModeloPerfil)
#    @nsPerfiles.expect(ModeloPerfil)
#    def post(self):
#        data = parserDatos.parse_args()
#        c = Cuenta(email=data.get('email'),contraseña=data.get('contraseña'))
#        perf = Perfil(pseudonimo=data.get('pseudonimo'),seguidores=0,seguidos=0,publicaciones=0)
#        pers = Persona(nombre=data.get('nombre'))
#        repoCuentas.agregar(c)
#        repoPerfiles.agregar(perf)
#        repoPersonas.agregar(pers)

#        return c,201

@nsPerfiles.route('/<int:id>')
class ClienteResource(Resource):
    @nsPerfiles.expect(ModeloIdPerfil)
    @nsPerfiles.marshal_with(ModeloPerfil)
    @nsPerfiles.param('<int:id>')
    def get(self,id):
        print(id)
        c = repoPerfiles.buscar_perfil(id)
        if c:
            return c,200
        return f'Perfil (id: {id}) no encontrado',404

#    @nsCuentas.param('<int:id>')
#    @nsCuentas.expect(ModeloIdCuenta)
#    def put(self,id):
#        data = parserEditarCuenta.parse_args()
#        repoCuentas.modificar(id,data)
#        return f'Cliente (id: {id}) modificado correctamente',200

#    @nsCuentas.param('<int:id>')
#    @nsCuentas.expect(ModeloIdCuenta)
#    def delete(self,id):
#        repoCuentas.borrar(id)
#        return f'Cliente (id: {id}) eliminado correctamente',200

