from flask import Flask
from flask_restx import Namespace,Resource,Model,fields,reqparse

from api.utils import ModeloCuenta,parserCuenta,ModeloIdCuenta,parserEditarCuenta,ModeloDatos,parserDatos
from dominio.cuenta import Cuenta
from dominio.perfil import Perfil
from dominio.persona import Persona
from dominio.datos import Datos
from infraestructura.cuentas_repo_sql import CuentasRepoSql
from infraestructura.perfiles_repo_sql import PerfilesRepoSql
from infraestructura.personas_repo_sql import PersonasRepoSql

nsCuentas = Namespace(
    'cuentas',description='Administracion de cuentas',path='/cuentas')

nsCuentas.models[ModeloCuenta.name] = ModeloCuenta

repoCuentas = CuentasRepoSql()
repoPerfiles = PerfilesRepoSql()
repoPersonas = PersonasRepoSql()



@nsCuentas.route('/')
class ClientesResource(Resource):
    @nsCuentas.marshal_list_with(ModeloDatos)
    def get(self):
        return repoCuentas.get_all()


    @nsCuentas.marshal_with(ModeloDatos)
    @nsCuentas.expect(ModeloDatos)
    def post(self):
        data = parserDatos.parse_args()
        c = Cuenta(email=data.get('email'),contraseña=data.get('contraseña'))
        perf = Perfil(pseudonimo=data.get('pseudonimo'),seguidores=0,seguidos=0,publicaciones=0)
        pers = Persona(nombre=data.get('nombre'))
        repoCuentas.agregar(c)
        repoPerfiles.agregar(perf)
        repoPersonas.agregar(pers)

        return c,201

@nsCuentas.route('/<int:id>')
class ClienteResource(Resource):
    @nsCuentas.expect(ModeloIdCuenta)
    @nsCuentas.marshal_with(ModeloCuenta)
    @nsCuentas.param('<int:id>')
    def get(self,id):
        print(id)
        c = repoCuentas.buscar_cuenta(id)
        if c:
            return c,200
        return f'Cliente (id: {id}) no encontrado',404

    @nsCuentas.param('<int:id>')
    @nsCuentas.expect(ModeloIdCuenta)
    def put(self,id):
        data = parserEditarCuenta.parse_args()
        repoCuentas.modificar(id,data)
        return f'Cliente (id: {id}) modificado correctamente',200

    @nsCuentas.param('<int:id>')
    @nsCuentas.expect(ModeloIdCuenta)
    def delete(self,id):
        repoCuentas.borrar(id)
        return f'Cliente (id: {id}) eliminado correctamente',200

@nsCuentas.route('/login')
class ClienteResourceII(Resource):
    @nsCuentas.expect(ModeloCuenta)
    @nsCuentas.marshal_with(ModeloCuenta)
    def put(self):
        data = parserCuenta.parse_args()
        
        return repoCuentas.get_login(data.get('email'),data.get('contraseña'))
        
        

        

        
        
        
